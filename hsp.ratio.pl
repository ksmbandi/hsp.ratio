#! /usr/bin/perl -w

# AUTHOR: Mbandi et al;
# LAST REVISED: December 2013
# Copyright (c) 2011 The Regents of christoffels_lab
#South African National Bioinformatics Institute

#This script is used to generate a tab delimited file of HSP ratios
#if amongst common proteins in the database that is used for tblastn
#The file will then be used for statistical analysis

use strict;
use warnings;
my $usage = "compare.top.hsp.tabble.pl top.hsp.table1 top.hsp.table2\n";
my $filea = $ARGV[0] or die "$usage";
my $fileb = $ARGV[1] or die "$usage";
my $filec = $ARGV[2] or die "$usage";
open(HSPA, $filea) || die "$filea is not accessible\n";
open(HSPB, $fileb) || die "$fileb is not accessible\n";
open(HSPC, $filec) || die "$filec is not accessible\n";
my (%hspa, %hspb, %hspc);
#my $delta = 0.0001;
while(my $linea = <HSPA>){
	chomp $linea;
	my @hita = split("\t", $linea);
	$hspa{$hita[0]} = $hita[8];
}
while(my $lineb = <HSPB>){
	chomp $lineb;
	my @hitb = split("\t", $lineb);
	if (defined $hspa{$hitb[0]}){
		$hspb{$hitb[0]} = "$hspa{$hitb[0]}\t$hitb[8]";	
		#print "$hitb[0]\t$hspa{$hitb[0]}\t$hitb[8]\n";
	}
}
while(my $linec = <HSPC>){
	chomp $linec;
	my @hitc = split("\t", $linec);
	if (defined $hspb{$hitc[0]}){
		print "$hitc[0]\t$hspb{$hitc[0]}\t$hitc[8]\n";
	}
}
close HSPA;
close HSPB;
close HSPC;
