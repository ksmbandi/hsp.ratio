#!/usr/bin/perl

# AUTHOR: Mbandi 
# LAST REVISED:June 2012
# Copyright (c) 2012 The Regents of christoffels_lab
#South African National Bioinformatics Institute
#mbandi@sanbi.ac.za
#Reducing noise in reconstructed transcripts 
#                A                    ------------------------------------------------------------------------------------------------------------------
#                B                                                                         ------------------------------------------------
#                D                               ---------------------------------------------------------------------------------
#                C                                                             -------------------------------------
#                E                                       ------------------
#Removes sequences which are substrings of other sequences in a fasta file (if  A,B,C,D are cognate sequences, then only A will retain because it is the largest).
#In otherwords; if B,D,C,E and truncated versions of A, only A will be retained 

use strict;
use warnings;
my $usage = <<EOU;
usage: removing_substring.pl infile one.line.fasta [Y|N]
The following parameters may be required:
        1. infile is a fasta file : Obligatory
	2. output: default: substring will be appended to input filename
EOU
#Check all legal inputs

if((@ARGV == 0) || (@ARGV > 2)) { 
	die "$usage";
}
my $target = $ARGV[0];
my $output;
unless  (@ARGV != 1){
	$output = "${target}.substring";
}
else {
	$output = $ARGV[1]
}
my $time = time;	
my $tempTARGET = "${target}.oneline";
&onelinefasta($target, $tempTARGET);
my %hash = ();
open(F, $tempTARGET)|| &ErrorMessage($tempTARGET);
while(my $id=<F>) {
		my $seq = <F>;
	chomp ($id, $seq);
	$id =~ s/^>//g;
	$hash{$id} = $seq;
}
close F; 

open(E, $tempTARGET)|| &ErrorMessage($tempTARGET);
my %new = ();
#print "start process $tempTARGET\n";
while(my $id2 = <E>) {
		my $seq2 = <E>;
		chomp ($id2, $seq2);
		while (my ($key, $value) = each (%hash)) {
			if(length($seq2) > length($hash{$key})) { #avoid matching exact strings
				my $rseq2 = reverse($seq2);
				my $rcseq2 = $rseq2;
				$rcseq2 =~ tr/A,T,C,G/T,A,G,C/;
				if ($seq2 =~  m/$value/) {                    
					delete$hash{$key};        #clean forward substring in memory 
				}
				elsif ($rcseq2 =~  m/$value/) {
                                        delete$hash{$key};        #clean reverse compliment substring in memory 
                                }
				else {
					#make coffee;
				}
			}
		}
}
close E; #print "Ready to open file\n";
open(O,"> $output") || &ErrorMessage($output);
foreach my $bigest_string (keys (%hash)) {
	print O ">", $bigest_string, "\n", $hash{$bigest_string}, "\n";
}
close O;
unlink $tempTARGET;
$time = time - $time;

if($time < 60) {
        print "Total time elapsed: $time secs.\n";
}
elsif (($time >= 60) && ($time < 3600)) {
        $time = $time/60;
        print "Total time elapsed: $time mins.\n";
}
else { 
        $time = $time/3600;
        print "Total time elapse: $time hours.\n";
}

sub ErrorMessage {
        my $msg = shift;
        print "Fatal error, there is no file with name: $msg\n";
        exit(1);        
}

sub does_file_exit {
        my ($exist_file) = @_;
        if (-e $exist_file) { die "FATAL ERROR:: Output file $exist_file already exist!\n";} else { ; } #Checks id temporal files have been deleted
}
#end of does_file_exist
sub onelinefasta {
        my ($in, $out) = @_;
        does_file_exit($out);
        open (IF, $in) || &ErrorMessage($in);
        open (TM, ">> $out") || &ErrorMessage($out);
        my $count = 0;
        my $sep = 1;
        while (my $line = <IF>){
                chomp $line;
                if ($line =~ m/^>/){
                        $count+=1;
                        if ($sep != $count){
                        print TM "\n"; 
                        } 
                        print TM $line."\n";
                }
                else{
                        unless($line =~ />/){         #stop if you encounter '>'
                                my $seq .= uc($line); #converts to uppercase and concatenate preceding strings 
                                print TM $seq;
                        }
                }
        } if ($count > 1){print TM "\n";}             #add a new line to the last sequence
close IF;
close TM;       
}
#End of one line fast 
