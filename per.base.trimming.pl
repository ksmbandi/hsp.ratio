#!/usr/bin/perl
#Mbandi
#Christoffels Lab
#SANBI
#mbandi@sanbi.ac.za
#based on the code snippets from ConDeTri 
#(Smeds, L., Künstner, A., 2011. ConDeTri - A Content Dependent Read Trimmer for Illumina Data. PLoS ONE 6, e26314.)
#filters reads base on a given length and quality threshold. 
#Writes reads of a pair to individual files if both pass the quality the threshold. 
#If only one member of a pair passes the threshold, read will be written to a file and single reads.
#

#filters reads base on a given length and quality threshold. Writes reads of a 
#pair to individual files if both pass the quality the threshold. If only one member 
#of a pair passes the threshold, read will be written to a file and single reads.

#This codes is amongst first of those I wrote for NGS related work
#I remember combing snippets from the internet.
#Provided with no gaurantee: use at your own peril

use strict;
use warnings;

if (@ARGV != 8) {
	#pirnts this message and note on usage
	print STDERR "\nusage: perl.base.trimming.pl fastq1 fast2 quality-type length-threshold quality-threshold outfile1 outfile2 single\n\n";
	print STDERR "There are three main variants of the quality type \nSanger\nilumina\nsolexa\n";
	exit(1);
}

my $fq1 = $ARGV[0];
my $fq2 = $ARGV[1];
my $type = $ARGV[2];
my $length_threshold = $ARGV[3];
my $qual_threshold = $ARGV[4];
my $read1 = $ARGV[5];
my $read2 = $ARGV[6];
my $read3 = $ARGV[7];
my $time = time;
open (PE, $fq1) || &ErrorMessage($fq1);
open (PF, $fq2) || &ErrorMessage($fq2);
open (RO, ">$read1") || die "cannot locate $read1";
open (RP, ">$read2") || die "cannot locate $read2";
open (RQ, ">$read3") || die "cannot locate $read3";
my ($char);
while (my $id1 = <PE>) {
	my $seq1 = <PE>;
	chomp $seq1; 
	my $plus1 = <PE>;
	my $qual1 = <PE>;
	chomp $qual1;
	my $cnt1 = 0;
	my @qual1 = split("", $qual1);
	foreach my $char(@qual1) {
		my $score1 = qual_value($char);
			if ($score1  < $qual_threshold) {
				last;	
			}
		$cnt1 ++;	
	}
	my $decision1 = 0;
	my $nseq1 = substr($seq1, 0, $cnt1);
	my $nqual1 = substr($qual1, 0, $cnt1);
	if ($cnt1 >= $length_threshold) {
		$decision1 = 1;
	}
	my $id2 = <PF>;
	my $seq2 = <PF>;
	chomp $seq2; 
	my $plus2 = <PF>;
	my $qual2 = <PF>;
	chomp $qual2;
	my $cnt2 = 0;
	my @qual2 = split("", $qual2);
	foreach my $char(@qual2) {
		my $score2 = qual_value($char);
			if ($score2  < $qual_threshold) {
				last;	
			}
		$cnt2 ++;	
	}
	my $decision2 = 0;
	my $nseq2 = substr($seq2, 0, $cnt2);
	my $nqual2 = substr($qual2, 0, $cnt2);
	if ($cnt2 >= $length_threshold) {
		$decision2 = 1;
	}
	if ($decision1 ==1 &&  $decision2 == 1){
		print RO "$id1$nseq1\n$plus1$nqual1\n";
		print RP"$id2$nseq2\n$plus2$nqual2\n";
	}
	elsif ($decision1 ==1 &&  $decision2 == 0){
		print RQ "$id1$nseq1\n$plus1$nqual1\n";
	}
	elsif ($decision1 == 0 &&  $decision2 == 1){
		print RQ "$id2$nseq2\n$plus2$nqual2\n";
	}
	else{
	next;
	}
}
close PE;
close PF;
close RO;
close RP;
close RQ;
$time = time - $time;
#I remember being interested in the time because we had little
#computing resoure at the time this was written
if($time < 60) {
	print "Total time elapsed: $time secs.\n";
}
elsif (($time >= 60) && ($time < 3600)) {
	$time = $time/60;
	print "Total time elapsed: $time mins.\n";
}
else { 
	$time = $time/3600;
	print "Total time elapse: $time hours.\n";
}

sub ErrorMessage {
	my $err = shift;
	return "Fatal error: $err does not exist\n";
	exit(1);	
}

sub qual_value {
	$char = shift;
	if  ($type eq 'sanger') {
	#Current values accepted are:
	#'sanger'   (orginal FASTQ)
	#ASCII encoding from 33-126, PHRED quality score from 0 to 93
	return (ord($char) - 33);
	}
	if ($type eq 'illumina') {
	#'solexa'   (aka illumina1.0)
	#ASCII encoding from 59-104, SOLEXA quality score from -5 to 40
	#'illumina' (aka illumina1.3)
	#ASCII encoding from 64-104, PHRED quality score from 0 to 40
	return (ord($char) - 64);
	}
	if ($type eq 'solexa') {
	#(Derived from the MAQ website):
	#For 'solexa', scores are converted to PHRED qual scores using:
	return (10 * log(1 + 10 ** ((ord($char) - 64) / 10.0)) / log(10));
	}
}
