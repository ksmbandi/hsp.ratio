#!/usr/bin/perl
use strict;
use warnings;
# AUTHOR: Mbandie 
# LAST REVISED:June 2012
# Copyright (c) May 2012 The Regents of christoffels_lab
#South African National Bioinformatics Institute
#mbandi@sanbi.ac.za
#Retains single copies of exact duplicates
#Removes  exact and/or reverse exact duplicate of preceding  sequences when looping across a fasta file
my $usage = <<EOU;
usage: remove_duplicates.pl infile one.line.fasta [Y|N]
The following parameters may be required:
        1. infile is a fasta file : Obligatory
	2. output: default: 'single.copy' will be appended to input filename
EOU
#Check all legal inputs
if((@ARGV == 0) || (@ARGV > 2)) { 
	die "$usage";
}
my $target = $ARGV[0];
my $normlize;
unless  (@ARGV != 1){
	$normlize = "${target}.single.copy";
}
else {
	$normlize = $ARGV[1];
}
my $tempTARGET = "${target}.oneline";
my $repeat = "${target}.repeated.sequences";
my $complement = "${target}.complement.sequences";
&onelinefasta($target, $tempTARGET);
my %hash;
my %count;

open (R, "> $repeat") || &ErrorMessage($repeat);
open (C, "> $normlize") || &ErrorMessage($normlize);
open (Y, "> $complement") || &ErrorMessage($complement);
my $trans = 0; my $rev_match = 0; my $dupl = 0;
open(X, $tempTARGET)|| &ErrorMessage($tempTARGET);
while(my $id = <X>) {
	$trans ++;
	my $seq = <X>;
	chomp ($id, $seq);
	$id =~ s/^>//g;
	$count{$seq} ++;
	if(defined $hash{$seq}) { #Check for exact  duplicates
		print R ">$hash{$seq}\n$seq\n";
		next; #Do not overwrite memory, skip to next sequence
	}
	my $revcompseq = $seq;
	$revcompseq =~ tr/A,T,C,G/T,A,G,C/;
	$revcompseq = reverse($revcompseq);
	if (defined ($hash{$revcompseq})){ #check for exact reverse complement duplicates
		$rev_match ++;
		print Y ">$id\n$seq\n";
		$hash{$seq} = $id;
		unless ($revcompseq eq $seq) { #palindrome reverse complement
			delete  $hash{$seq};
			
		}
		
		next;
	}
	$hash{$seq} = $id;  
}
close X;
close R;
unlink $tempTARGET;
while ((my $key, my $value) = each %hash){
	print  C ">$hash{$key}\n$key\n";
}
my $dup = 0; my $unique = 0; 
for my $seqQ(keys %count){#print $count{$seqQ}."\n";
	if($count{$seqQ} > 1){
		$dup ++;
	}
	if($count{$seqQ} == 1){
		$unique ++;
	}
}
close C;
my $without_duplicate = (($unique + $dup) - $rev_match);
print STDOUT "Number of sequences: $trans\nNumber of unique sequences: $unique\nNumber of duplicate sequences: $dup\nNumber of reverse duplicate sequences: $rev_match\nNumber of sequnces without  duplicate: $without_duplicate\n";
sub onelinefasta {
	my ($in, $out) = @_;
	does_file_exit($out);
	open (IF, $in) || &ErrorMessage($in);
	open (TM, ">> $out") || &ErrorMessage($out);
	my $count = 0;
	my $sep = 1;
	while (my $line = <IF>){
		chomp $line;
		if ($line =~ m/^>/){
			$count+=1;
			if ($sep != $count){
			print TM "\n"; 
			} 
			print TM $line."\n";
		}
		else{
			unless($line =~ />/){
				my $seq .= uc($line);
				print TM $seq;
			}
		}
	} if ($count > 1){print TM "\n";}
close IF;
close TM;	
}
#end of onelinefasta 
sub does_file_exit {
	my ($exist_file) = @_;
        if (-e $exist_file) { die "FATAL ERROR:: Output file $exist_file already exist!\n";} else { ; }
}
#end of does_file_exit
sub ErrorMessage {
	my $msg = shift;
	print "Fatal error: $msg\n";
	exit(1);        
}
