#!/bin/perl -w

#AUTHOR: Stanley Mbandi-Kimbung
#LAST REVISED: June 2012
#Copyright (c) 2011 The Regents of christoffels_lab
#South African National Bioinformatics Institute
#christoffels_lab@sanbi.ac.za
#-----------------------------------------------------------------------------------------------
#Computes assembly attributes.
#№ transfags;	№ transfags > N50;	№ transfags > mean;	max;	median;	mean;	N50
use strict;
use warnings;
if (@ARGV != 1) {
        print STDERR "\nusage: fasta.summary.stats.pl  fasta\n\n";     
        exit(1);
}
my ($tot_len,  $num_reads, $count,  $half,  $n50, $bigmediantrans,  $bignmeantrans, $median, $i, $length) =(0,0,0,0,0,0,0,0,0,0); 
my @rnatigs;
my $fasta = shift;
open(I, $fasta) || die "$fasta is non-existent\n";
while(my $line = <I>){
	chomp $line;
	$line =~ s/\s//g;
  	 if($line =~ m/^\>/){
		$num_reads ++;
		push (@rnatigs, $length);
		$tot_len += $length;
		$length=0;
		next;
	}
	$length += length($line);
}
#--------------------------------------------------------------------
#Prevent division by 0 error in case you get junk data
#--------------------------------------------------------------------
return undef unless(scalar(@rnatigs));
my @transfrags = sort { $b <=> $a } @rnatigs;
#print "@transfrags". "\n";
#--------------------------------------------------------------------
# computes mean of the rnatigs
#--------------------------------------------------------------------
my $average = int ($tot_len / $num_reads);
#---------------------------------------------------------------------
#computes the median of the set of rnatigs
#----------------------------------------------------------------------
if ( $num_reads % 2) {
        $median = $transfrags[int($num_reads/2)];
	} 
else {
        $median = int(($transfrags[$num_reads/2] + $transfrags[$num_reads/2 - 1]) / 2);
}
#-----------------------------------------------------------------------------
#computes N50
#-----------------------------------------------------------------------------
for (my $index = 0; $index < @transfrags; $index++){
        $count += $transfrags[$index];
        if (($count >= $tot_len/2) && ( $half==0 )){ 
                $n50 = $transfrags[$index];
                $half = $transfrags[$index]; 
        }
}
while ($i<=$#transfrags){
	if( $transfrags[$i] >= $median){
		$bigmediantrans ++; #number of transfrags >= median sequence length
		}
	if ($transfrags[$i] >= $average){
		$bignmeantrans ++;  #Number of transfrags >= mean sequence length
		}
	$i++;
} 
#print $bigmediantrans."\t".$bignmeantrans."\t".$transfrags[0]. "\t".$median."\t".$average."\t".$n50."\t".$num_reads."\n";
print $num_reads."\t".$bigmediantrans."\t".$bignmeantrans."\t".$transfrags[0]."\t".$median."\t".$average."\t".$n50."\n";
#print $num_reads."\t".$transfrags[0]."\t".$median."\t".$average."\t".$bigmediantrans."\t".$bignmeantrans."\n";

