#!/usr/bin/perl
#
#converts a fastQ file to a fastA file
#created 16-02-2012
#last modified: 16-02-2012
use strict;
use warnings;

if (@ARGV != 1) {
	print STDERR "\nusage: fastq_to_fasta.pl  fastq_file\n\n";	
	exit(1);
}

my $fq = $ARGV[0];
open (PE, $fq) || &ErrorMessage("Cannot open file ". $fq);
my @len =( );
my $num_reads =0;
my $sum = 0;

while (my $id = <PE>) {
	if ($id =~ m/^\@/) {
		$id =~ s/\@/>/;
		my $seq = <PE>;
		<PE>;
		<PE>;
		print $id;
		print $seq;
		}
	else {
		print "$fq is not a standard fastq file\n";
		exit(1);
	}	
}

sub ErrorMessage {
	my $err = shift;
	print STDERR "Fatal error: $err\n";
	exit(1);	
}
