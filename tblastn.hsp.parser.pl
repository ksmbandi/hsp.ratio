#! /usr/bin/perl -w

# AUTHOR: Mbandi
# LAST REVISED: January 2012
# Copyright (c) 2011 The Regents of christoffels_lab
#South African National Bioinformatics Institute
#mbandi@sanbi.ac.za

use strict;
use warnings;
use Bio::SearchIO; 
my $usage = "tblastn.hsp.parser.pl blastoutput.xml.or.raw.format\n";
my $file = $ARGV[0] or die "$usage";
my $in = new Bio::SearchIO(-tempfile => 1, -format => 'blastxml', -file   => $file);
my %hsp;
my ($delta) = 0.0001;
while( my $result = $in->next_result ) {
	while( my $hit = $result->next_hit ) {
		while( my $hsp = $hit->next_hsp ) {
			my $qname = $result->query_name;
			my $qlength = $result->query_length;
			my $hname = $hit->name;
			my $hlength = $hit->length;
		 	my $hsplength = $hsp->length('query'); #returns the number of amino acids in the hit without gaps
			#my $Percent_id = $hsp->percent_identity;
			my $frac_identity = $hsp->frac_identical; #Drop since we are blast against sequences from different species
			my $bit_score = $hsp->bits;
			my $hspeval = $hsp->evalue;
			#my $num_conserved = $hsp->num_conserved;
			#my $hspcov = (($hsplength/$qlength)*100); #since protein is a reference and used as the query, we are interested in its coverage
			my $frachspcov = ($hsplength/$qlength);
			if (( $frachspcov > 0.25) || abs ($frachspcov - 0.25) < $delta){
				print $qname."\t".$qlength."\t".$hname."\t".$hlength."\t".$hsplength."\t".$frac_identity."\t".$hspeval."\t".$bit_score."\t".$frachspcov."\n";
			}
		}
	}  
}
