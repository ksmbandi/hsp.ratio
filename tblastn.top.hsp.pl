#!/usr/bin/perl

# AUTHOR:   Stanley Mbandi-Kimbung & Ujana Hesse 
# LAST REVISED:August 2012
# Copyright (c) 2012 The Regents of christoffels_lab
#mbandi@sanbi.ac.za

#This script enables you to get the best hsp to all hits in the tab file
use strict;
use warnings;

#print STDOUT "\nThis code will print out best hit match to a query\nInput file must be sorted by query name\n";
my $usage = "tblastn.top.hsp.pl blastoutput.hsp.table\n";
my $file = $ARGV[0] or die "$usage";
my (%query, %hsp_per_query, $value);
open(F, $file) || &ErrorMessage ($file);;
HIT:
while (my $line = <F>) {
	chomp $line;
        my @result = split("\t", $line);
        #print $result[0]."\n";
        if (exists $query{$result[0]}) {
                $hsp_per_query{$result[0]} ++;
        #$value =  join("\|", $query{$result[0]}, $result[2]);
				#$query{$result[0]} = $value; #update $line by
        }
        else {
                $query{$result[0]} = $line;
                $hsp_per_query{$result[0]} ++;
	}
}

foreach my $key ( sort keys %query ) {
	#if ($hsp_per_query{$key} == 1){
		print $query{$key}."\t".$hsp_per_query{$key}."\n";
	#}
}

close F;

sub ErrorMessage {
     	   my $msg = shift;
    	    #print "Fatal error, there is no file with name: $msg\n";
		die "\nThe filename:$msg does not exist.\nCheck that you've provided the appropriate path.\n\n";
   		     #exit(1);        
}
