# AUTHOR: Mbandi
# LAST REVISED: created December 2013
# Copyright (c) 2011 The Regents of christoffels_lab
#South African National Bioinformatics Institute
#mbandi@sanbi.ac.za

	
frac<-read.table("coverage.txt", header=F, sep="\t")
	fracov<-list(frac$V2,  frac$V3, frac$V4)
	names(fracov)<-c('untrimmed', 'trimmedQ10', 'trimmedQ20')
	d<-stack(fracov)
	kruskal.test(values~ind,data=d)
	library(agricolae)
	kruskal(d$values, d$ind, group=TRUE, p.adj="holm", alpha = 0.01)
