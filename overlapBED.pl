#!/usr/bin/perl
use strict;
use warnings;
##############################################################################################
#		Author: Mbandi Sk                                                            #
#		Reagent of the Christoffels lab @ SANBI                                      #
#		To invesgate potentially novel gene loci                                     #
#		Tool for comparing genomic festures   (requires BED files)                   #
#		                                                                             #
#		Motivation: w/o strandedness, bedtool reports non-overlaping features as     #
#		overlap which makes it unsuitable at finding overlapping                     #
#		transfrags on a reference genome.                                            #
#TO DO: in the future I will add overlap threshold and strandedness                          #
##############################################################################################

my $refbed = shift || die "provide bedfile1\n";
my $querybed = shift || die "provde bedfile2\n";
my %bed = ();
#system "export PATH=\$PATH:/cip0/software/x86_64/bedtools/bin/";
open(R, $refbed);
my $cnt_pair = 0;
while(<R>){
	my $line = $_;
	my @chrom = split("\t", $line);
	my $chromsome = $chrom[0]; #print $chromsome."\n";
	my @query = &add_query_bed();
	#print $line.$query;
	my $refid = $chrom[3];
	my $rstart = $chrom[1];
	my $rstop = $chrom[2];
	foreach my $query (@query){
		$cnt_pair ++;
		my @queryid = split("\t", $query);
		my $qchromosome = $queryid[0];
		my $queryid = $queryid[3];
		my $qstart = $queryid[1];
		my $qstop = $queryid[2];
		#print "$rstart\t$rstop\n$qstart\t$qstop\n";
		if ($chromsome eq $qchromosome){#features would only overlap if both exist on the same chromosome(contig or scaffold)
			if (($rstop >= $qstart) && ($rstart <= $qstop)){ # reference precedes query
				print "$chromsome\t$refid\t$queryid\t$rstart\t$qstop\n";
			}
			elsif (($qstop >= $rstart) && ($qstart <= $rstop)){ # query precedes reference
				print "$chromsome\t$refid\t$queryid\t$qstart\t$rstop\n"
			}
			else{
				#express silence or make no utterances
			}
		}
	}
}
print STDOUT "Number of evaluated pairs: $cnt_pair\n";
sub add_query_bed{
	#iterates over second bed file.
	my @entry;
	open(Q, $querybed) || die "provde query bedfile\n";
	while (my $entry = <Q>){
		chomp $entry; #return $entry."\n";
		push (@entry, $entry);
	}#print @entry;exit;
	return @entry;
	close Q;
}
