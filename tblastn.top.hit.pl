#!/usr/bin/perl

# AUTHOR:   Stanley Mbandi-Kimbung & Ujana Hesse 
# LAST REVISED:August 2012
# Copyright (c) 2012 The Regents of christoffels_lab
#South African National Bioinformatics Institute

#This script enables you to get the best hsp to all hits in the tab file
use strict;
use warnings;

#print STDOUT "\nThis code will print out best hit match to a hit\nInput file must be sorted by hit name\n";
#sorting was performed as follows: sort -t $'\t' -k 3,3 -k 7n,7 -k 8rn,8 trinity.unprocessed.top.hsp > trinity.unprocessed.top.hsp.sort
my $usage = "tblastn.top.hit.pl blastoutput.hsp.table.sort\n";
my $file = $ARGV[0] or die "$usage";
my (%hit, %hsp_per_hit, $value);
open(F, $file) || &ErrorMessage ($file);;
HIT:
while (my $line = <F>) {
	chomp $line;
        my @result = split("\t", $line);
        #print $result[0]."\n";
        if (exists $hit{$result[2]}) {
                $hsp_per_hit{$result[2]} ++;
        }
        else {
                $hit{$result[2]} = $line;
                $hsp_per_hit{$result[2]} ++;
	}
}

foreach my $key ( sort keys %hit ) {
	#if ($hsp_per_hit{$key} == 1){
		print $hit{$key}."\t".$hsp_per_hit{$key}."\n";
	#}
}

close F;

sub ErrorMessage {
     	   my $msg = shift;
    	    #print "Fatal error, there is no file with name: $msg\n";
		die "\nThe filename:$msg does not exist.\nCheck that you've provided the appropriate path.\n\n";
   		     #exit(1);        
}
